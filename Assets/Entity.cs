﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Universe.World {
	public class Entity : MonoBehaviour {
		public Guid ID { get; private set; }
		public void Awake() {
			ID = Guid.NewGuid();
		}
		public void Start() {
			Universe.Entities.Add(ID,this);
		}

		public void Interact(Entity other, string action) {

		}
	}
}