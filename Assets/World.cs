﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Universe.World {
	public class World : MonoBehaviour {
		public Entity SpawnEntity(Entity entity) {
			Universe.Entities.Add(entity.ID,entity);
			return entity;
		}
	}
}