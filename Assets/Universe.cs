﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Universe.World;

namespace Universe {
	public class Universe {
		public delegate void DataRequestDelegate(Data final);
		//public static DataRequestDelegate DataRequest = UniverseRequest;
		// eventually
		// DataRequest += KnapsackRequest
		// DataRequest += WorldRequest
		// DataRequest += etcRequest

		public static World.World World;
		public static System.Random Random = new System.Random();
		public static Dictionary<Guid,Entity> Entities = new Dictionary<Guid,Entity>();

		public static Data RequestData() { // base request data
			Data data = new Data();
			UniverseRequest(data);
			return data; // aaaaa
		}
		public static Data RequestData<T>(string key, T entry) {
			Data data = RequestData();
			data.Add(key,entry);
			return data;
		}
		public static Data RequestData<T1, T2>(string key1,T1 entry1,string key2,T2 entry2) {
			Data data = RequestData(key1,entry1);
			data.Add(key2,entry2);
			return data;
		}
		public static Data RequestData<T1,T2,T3>(string key1,T1 entry1,string key2,T2 entry2,string key3,T3 entry3) {
			Data data = RequestData(key1, entry1, key2, entry2);
			data.Add(key3,entry3);
			return data;
		}

		public static void UniverseRequest(Data final) {
			final.Add("world",World);
			final.Add("entities",Entities);
		}
	}
	public class Data {
		public Dictionary<string,IDataEntry> Entries { get; protected set; }
		public Data() {
			Entries = new Dictionary<string,IDataEntry>();
		}
		public void Add<T>(string key,T value) {
			Entries.Add(key,new DataEntry<T>(value));
		}
		public T Read<T> (string key) {
			if (Entries[key] is DataEntry<T>) {
				return ((DataEntry<T>)Entries[key]).Value;
			}
			return default(T);
		}
	}
	public class DataEntry<T> : IDataEntry {
		public T Value { get; set; }
		public DataEntry (T value) {
			Value = value;
		}
	}
	public interface IDataEntry { }
}
