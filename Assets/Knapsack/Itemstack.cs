﻿using System;
using System.Collections.Generic;
using Knapsack.Items;

namespace Knapsack {
	public class ItemStack : IComparable<ItemStack>{

		// Stored ItemType
		public BaseItem ItemType { get; protected set; }

		// "parent" inventory
		public Inventory Holder;

		public int Quantity { get; protected set; }

		public PropertyHolder Properties { get; set; }

		// Default constructor creates empty itemstack
		public ItemStack() {
			Quantity = 0;
			ItemType = null;
		}

		public ItemStack(BaseItem itemType,int quantity) {
			ItemType = itemType;
			Properties = itemType.Properties.Copy();
			if(itemType != null) {
				Set(quantity);
			}
		}

		public bool Empty() {
			return Quantity <= 0 || ItemType == null;
		}

		public bool Full() {
			return !Empty() && Quantity >= ItemType.StackSize;
		}

		public void Set(int quantity) {
			if(quantity > ItemType.StackSize) {
				Quantity = ItemType.StackSize;
			} else if(quantity < 0) {
				Quantity = 0;
				Nullify();
			} else {
				Quantity = quantity;
			}
		}

		public void Add(int amount) {
			if(Empty())
				return;
			if(amount < 0) {
				Remove(-amount);
				return;
			}
			Set(Quantity + amount);
		}

		public void Remove(int amount) {
			if(Empty())
				return;
			if(amount < 0) {
				Add(-amount);
				return;
			}
			Set(Quantity - amount);
		}

		public void Nullify() {
			ItemType = null;
		}

		// transfer the amount specified from thisItem into otherItem
		// positive amount values add onto other
		// negative amount values remove from other
		public void Transfer(ItemStack other,int amount) {
			if(amount == 0 ||
				Quantity == 0 ||
				!Equivalent(other))
				return;
			if(amount < 0) {
				Transfer(other,-amount);
				return;
			}
			int transferAmount = amount;

			int[] check = { other.ItemType.StackSize - other.Quantity,Quantity,amount };
			for(int i = 0; i < check.Length; i++) {
				transferAmount = Math.Min(check[i],transferAmount);
			}

			Remove(transferAmount);
			other.Add(transferAmount);
		}

		// Merges this itemstack with another; if this itemstack has 0 items at the end of the operation it becomes empty
		public void Merge(ItemStack other) {
			if (other.ItemType == null) {
				return;
			}
			if(Equivalent(other)) {
				Transfer(other,Quantity);

				if(Quantity == 0) {
					Nullify();
				}
			}
		}

		// Splits this itemstack into two and returns the new one
		public ItemStack Split() {
			if(Quantity == 1)
				return new ItemStack();
			ItemStack copy = Copy();
			int copyQuantity = (int)Math.Floor(Quantity / 2f);

			Set(Quantity - copyQuantity);
			copy.Set(copyQuantity);

			return copy;
		}

		public ItemStack Copy() {
			ItemStack copy = new ItemStack(ItemType,Quantity);
			return copy;
		}

		public bool Equivalent(ItemStack other) {
			if(Empty() || other.Empty())
				return false;	
			return ItemType.GetType() == other.ItemType.GetType() && Properties.Equivalent(other.Properties);
		}

		public string Name() {
			return Empty() ? "Empty" : ItemType.Name;
		}

		public override string ToString() {
			if(Empty()) {
				return "Empty";
			}
			string properties = Properties.Count > 0 ? "- [Properties]\n" : "";
			foreach(KeyValuePair<string,ItemProperty> entry in Properties) {
				properties += string.Format("  - {0}: {1}\n",entry.Key,entry.Value);
			}
			return string.Format("({0}/{1}) {2}\n"+properties,Quantity,ItemType.StackSize,ItemType);
		}

		public int CompareTo(ItemStack other) {
			if(other.Empty()) {
				if(Empty())
					return 0;
				return -1;
			} else if (Empty()) {
				return 1;
			}

			Type thisType = ItemType.GetType();
			Type otherType = other.ItemType.GetType();

			int classComp = ItemType.Depth().CompareTo(other.ItemType.Depth()); // lower is better
			if (classComp != 0) // class depth is the same
				return classComp;

			Type[] thisInter = thisType.GetInterfaces();
			Type[] otherInter = otherType.GetInterfaces();
			int IComp = thisInter.Length.CompareTo(otherInter.Length);
			if (IComp != 0) // interface count is the same
				return IComp;
			
			for (int i = 0; i < thisInter.Length; i++) {
				int intComp = thisInter[i].Name.CompareTo(otherInter[i].Name);
				if(intComp != 0) // all interfaces are identical
					return intComp;
			}

			int baseComp = thisType.BaseType.Name.CompareTo(otherType.BaseType.Name);
			if(baseComp != 0) // base class names are the same
				return baseComp;

			int classNameComp = thisType.Name.CompareTo(otherType.Name);
			if(classNameComp != 0) // class names are the same
				return classNameComp;

			int quantComp = -Quantity.CompareTo(other.Quantity);
			if(quantComp != 0) // quantities are the same
				return quantComp;

			foreach(KeyValuePair<string,ItemProperty> entry in Properties) { // if each of the properties are the same
				if(other.Properties.Contains(entry.Key)) {
					int propQuant = entry.Value.CompareTo(other.Properties.Read(entry.Key));
					if(propQuant != 0) // if they're the same move on to the next property
						return propQuant;
				} else {
					return 1; // property mismatch, this is greater
				}
			}
			return 0; // the item stacks are identical
		}
	}
}