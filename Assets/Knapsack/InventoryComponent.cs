﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Knapsack;
using Knapsack.Items;

public abstract class InventoryComponent : MonoBehaviour {
	public virtual Inventory Source { get; protected set; }
	public virtual void Awake() {
		//Source = new Inventory(new Inventory.SlotGroup<BaseItem>(5));
	}
}
