﻿using System;
using System.Collections;
using System.Collections.Generic;
using Knapsack.Items;

namespace Knapsack {
	public class Inventory : IEnumerable {
		public List<ItemStack> Items;
		public List<Type> SlotRestrictions;

		public readonly bool Resizable;

		public Inventory(int size,bool resize) {
			Resizable = resize;
			Items = new List<ItemStack>();
			SlotRestrictions = new List<Type>();
			for(int i = 0; i < size; i++) {
				Items.Add(new ItemStack());
				SlotRestrictions.Add(typeof(BaseItem));
			}
		}

		public Inventory(params ISlotGroup[] slots) {
			Resizable = false;
			Items = new List<ItemStack>();
			SlotRestrictions = new List<Type>();
			for (int i = 0; i < slots.Length; i++) {
				for (int s = 0; s < slots[i].Size; s++) {
					Items.Add(new ItemStack());
					SlotRestrictions.Add(slots[i].Type);
				}
			}
		}

		public struct SlotGroup<T> : ISlotGroup where T : BaseItem {
			public int Size { get; set; }
			public Type Type { get; set; }
			public SlotGroup(int size) {
				Size = size;
				Type = typeof(T);
			}
		}

		public interface ISlotGroup {
			int Size { get; set; }
			Type Type { get; set; }
		}

		public IEnumerator GetEnumerator() {
			return Items.GetEnumerator();
		}

		public int Count {
			get {
				return Items.Count;
			}
		}

		public ItemStack this[int slot] { // NO set, only get
			get {
				return Items[slot];
			}
			protected set {
				Items[slot] = value;
			}
		}

		public int GetSlot(ItemStack stack) {
			return Items.IndexOf(stack);
		}

		public int AvailableSlots<T> () where T : BaseItem {
			int slots = 0;
			for (int i = 0; i < Count; i++) {
				if(Items[i].Empty() && (SlotRestrictions[i] == typeof(T) || typeof(T).IsSubclassOf(SlotRestrictions[i]))) {
					slots++;
				}
			}
			return slots;
		}

		// returns the first available slot of type T, or -1 if there are none
		public int AvailableSlot<T>() where T : BaseItem {
			for(int i = 0; i < Count; i++) {
				if(Items[i].Empty() && (SlotRestrictions[i] == typeof(T) || typeof(T).IsSubclassOf(SlotRestrictions[i]))) {
					return i;
				}
			}
			return -1;
		}

		// expands inventory capacity by a positive amount
		public bool Expand(int amount = 1) {
			return Expand<BaseItem>(amount);
		}

		public bool Expand<T>(int amount = 1) where T : BaseItem {
			if(!Resizable)
				return false;
			for(int i = 0; i < amount; i++) {
				Items.Add(new ItemStack());
				SlotRestrictions.Add(typeof(T));
			}
			return true;
		}

		public bool ChangeSlot<T>(int slot) where T : BaseItem {
			if(!ValidSlot(slot))
				return false;
			if(Items[slot].Empty() || Items[slot].ItemType.Is(typeof(T))) {
				SlotRestrictions[slot] = typeof(T);
				return true;
			}
			return false;
		}

		// contracts inventory capacity by amount if last [amount] slots are empty
		public bool Contract(int amount) {
			if(!Resizable)
				return false;
			for(int i = 0; i < amount; i++) {
				if(Items[Count - 1].Empty()) {
					Items.RemoveAt(Count - 1);
				} else {
					return false;
				}
			}
			return true;
		}

		public int FirstEquivalent(ItemStack stack) {
			for (int i = 0; i < Count; i++) {
				if (Items[i].Equivalent(stack)) {
					return i;
				}
			}
			return -1;
		}
		public int FirstEquivalentNotFull(ItemStack stack) {
			return FirstEquivalentNotFullRange(stack,0,Count);
		}
		public int FirstEquivalentNotFullRange(ItemStack stack, int start, int end) {
			for(int i = 0; i < Count; i++) {
				if(Items[i].Equivalent(stack) && !Items[i].Full() && i >= start && i <= end) {
					return i;
				}
			}
			return -1;
		}

		public bool Deposit(ItemStack stack) {
			return Deposit<BaseItem>(stack);
		}

		public bool Deposit<T>(ItemStack stack) where T : BaseItem {
			return DepositRange<T>(stack,0,Count-1);
		}

		public bool DepositRange<T>(ItemStack stack, int start, int end) where T : BaseItem{
			ItemStack working = stack;
			int currentSlot = FirstEquivalentNotFullRange(stack, start, end);
			while(currentSlot != -1 && !working.Empty()) {
				working.Merge(Items[currentSlot]);
				if(Items[currentSlot].Full()) {
					currentSlot = FirstEquivalentNotFullRange(stack, start, end);
				}
			}
			if(!working.Empty()) {
				return Place<T>(stack);
			}
			return false;
		}

		// places the stack into the first empty slot
		public bool Place(ItemStack stack) {
			return Place<BaseItem>(stack);
		}

		public bool Place<T> (ItemStack stack) where T : BaseItem {
			return Insert(stack,AvailableSlot<T>());
		}

		// places the stack into the specified slot (if empty)
		public bool Insert(ItemStack stack,int slot) {
			if(!ValidSlot(slot))
				return false;
			if(Items[slot].Empty() && (stack.Empty() || stack.ItemType.Is(SlotRestrictions[slot]))) {
				if (!stack.Empty())
					stack.ItemType.OnBeforePlaced(Knapsack.RequestData(stack));
				Items[slot] = stack;
				stack.Holder = this;
				if(!stack.Empty())
					stack.ItemType.OnAfterPlaced(Knapsack.RequestData(stack));
				return true;
			}
			return false;
		}

		public void Scatter(params ItemStack[] stacks) {
			int max = Math.Min(stacks.Length,AvailableSlots<BaseItem>());
			for (int i = 0; i < max; i++) {
				Place(stacks[i]);
			}
		}

		public void Sort() {
			Sort(new HashSet<Type>());
		}

		private List<ItemStack> tempSort = new List<ItemStack>();
		// use a hashset to quickly check existence of items
		public void Sort(HashSet<Type> exclude) {
			// here we go
			int startInd = 0, endInd = 0;

			while (startInd < Count) { // loop through the entire inventory
				Type current = SlotRestrictions[startInd]; // store the current restriction defined for the first slot of the iteration
				for(endInd = startInd; endInd < Count && SlotRestrictions[endInd] == current; endInd++) {
				} // find the last concurrent slot that's the same type and store it

				endInd--; // account for last slot increasing by one too many
				for (int i = startInd; i <= endInd; i++) { // populate the temporary list while emptying the range
					tempSort.Add(Remove(i));
				}
				while (tempSort.Count > 0) { // empty out the temporary list, depositing items back into the main list
					DepositRange<BaseItem>(tempSort[tempSort.Count - 1],startInd,endInd);
					tempSort.RemoveAt(tempSort.Count - 1);
				}

				if(!exclude.Contains(current)) // if the current type is not to be excluded from sorting
					Items.Sort(startInd,endInd-startInd,Comparer<ItemStack>.Default); // sort the items list based on the range found
				
				startInd = endInd+1; // prepare the next iteration by setting the start index to one past the end
			}
		}

		// returns the item currently in the slot and removes it from the inventory
		public ItemStack Remove(int slot) {
			ItemStack toReturn = Items[slot];
			if(!ValidSlot(slot))
				return null; // can't remove what doesn't exist, dummy
			if(toReturn.Empty())
				return new ItemStack();
			toReturn.ItemType.OnBeforeRemoved(Knapsack.RequestData(toReturn));
			toReturn.Holder = null;
			Items[slot] = new ItemStack();
			toReturn.ItemType.OnAfterRemoved(Knapsack.RequestData(toReturn));
			return toReturn;
		}

		public ItemStack RemoveFirst() {
			for(int i = 0; i < Count; i++) {
				if(!Items[i].Empty()) {
					return Remove(i);
				}
			}
			return null;
		}

		public ItemStack RemoveLast() {
			for(int i = Count - 1; i >= 0; i--) {
				if(!Items[i].Empty()) {
					return Remove(i);
				}
			}
			return null;
		}

		public void Transfer(int slot1,int slot2,int amount) {
			Transfer(this,slot1,slot2,amount);
		}
		public void Transfer(Inventory other,int slot1,int slot2,int amount) {
			if(!ValidSlot(slot1) ||
				!other.ValidSlot(slot2) ||
				(other == this && slot1 == slot2))
				return;

			Items[slot1].Transfer(other.Items[slot2],amount);
		}

		public void Swap(int slot1,int slot2) {
			Swap(this,slot1,slot2);
		}

		// swap stacks between two inventories
		public void Swap(Inventory other,int slot1,int slot2) {
			if(!ValidSlot(slot1) ||
				!other.ValidSlot(slot2) ||
				(other == this && slot1 == slot2))
				return;
			ItemStack stack1 = Remove(slot1);
			ItemStack stack2 = other.Remove(slot2);

			Insert(stack2,slot1);
			other.Insert(stack1,slot2);
		}

		public void Split(int slot1,int slot2) {
			Split(this,slot1,slot2);
		}
		public void Split(Inventory other,int slot1,int slot2) {
			if(!ValidSlot(slot1) ||
				!other.ValidSlot(slot2) ||
				(other == this && slot1 == slot2) ||
				!other.Items[slot2].Empty())
				return;
			ItemStack copy = Items[slot1].Split();
			other.Insert(copy,slot2);
		}

		public void Merge(int slot1,int slot2) {
			Merge(this,slot1,slot2);
		}

		public void Merge(Inventory other,int slot1,int slot2) {
			if(!ValidSlot(slot1) ||
				!other.ValidSlot(slot2) ||
				(other == this && slot1 == slot2))
				return;

			Items[slot1].Merge(other.Items[slot2]);
		}

		public void Increase(int slot,int amount) {
			if(!ValidSlot(slot))
				return;
			Items[slot].Add(amount);
		}

		public void Decrease(int slot,int amount) {
			if(!ValidSlot(slot))
				return;
			Items[slot].Remove(amount);
		}

		public bool ValidSlot(int slot) {
			return slot >= 0 && slot < Count;
		}

		public override string ToString() {
			int maxLength = 0;
			for(int i = 0; i < Count; i++) {
				BaseItem item = Items[i].ItemType;
				maxLength = Math.Max(maxLength,(Items[i].Empty() ? 5 : item.Name.Length) + 4);
			}
			string format = "slot: {0, -4} item: {1, -" + maxLength + "} quant: {2, -8}";

			string toReturn = "";
			for(int i = 0; i < Count; i++) {
				if(this[i].Empty()) {
					toReturn += string.Format(format,i,"Empty","None");
				} else {
					toReturn += string.Format(format,i,this[i].ItemType.Name,this[i].Quantity);
				}
				if (SlotRestrictions[i] != typeof(BaseItem)) {
					toReturn += string.Format("type: {0}",SlotRestrictions[i].Name);
				}
				toReturn += "\n";
			}
			return toReturn;
		}
	}
}