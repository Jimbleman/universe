﻿using Universe;
namespace Knapsack.Attributes {
	public static class IDamageableHelper {
		public static void OnDestroyedDefault(this IDamageable damageable,Data data) {
			data.Read<ItemStack>("stack").Nullify();
		}
		public static void DamageDefault(this IDamageable damageable,Data data,int amount) {
			ItemStack stack = data.Read<ItemStack>("stack");
			PropertyHolder holder = stack.Properties;
			System.Console.WriteLine("Slot: " + stack.Holder.GetSlot(stack));
			int durability = holder.Read<int>("durability");
			durability -= amount;
			holder.Write("durability",durability);
			if (durability <= 0) {
				damageable.OnDestroyed(data);
			}
		}
	}
}
