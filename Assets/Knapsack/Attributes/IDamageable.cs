﻿using Universe;
namespace Knapsack.Attributes {
	public interface IDamageable {
		int MaxDurability { get; }

		void Damage(Data data, int amount);
		void OnDestroyed(Data data);
	}
}