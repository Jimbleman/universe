﻿using Universe;
namespace Knapsack.Attributes {
	public interface IEquippable {
		void OnEquip(Data data);
		void OnUnequip(Data data);
	}
}
