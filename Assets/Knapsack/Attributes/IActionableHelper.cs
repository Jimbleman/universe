﻿using System;
using System.Collections.Generic;
using Universe;

namespace Knapsack.Attributes {
	public static class IActionableHelper {
		public static void InvokeDefault(this IActionable actionable, string key, Data data) {
			Console.WriteLine("invoke");
			Action<Data> action;
			if(actionable.Actions.TryGetValue(key,out action)) {
				action.Invoke(data);
			}
		}
		public static string ToStringDefault(this IActionable actionable, string baseString) {
			string toReturn = baseString.ToString() + "\n- [Actions]";
			foreach(KeyValuePair<string,Action<Data>> action in actionable.Actions) {
				toReturn += "\n  - " + action.Key;
			}
			return toReturn;
		}
	}
}
