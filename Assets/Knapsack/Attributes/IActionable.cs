﻿using System;
using System.Collections.Generic;
using Universe;

namespace Knapsack.Attributes {
	public interface IActionable {
		Dictionary<string,Action<Data>> Actions { get; }

		void Invoke(string key,Data data);
	}
}