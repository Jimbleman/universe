﻿using System;
using System.Collections.Generic;
using Knapsack.Attributes;
using Universe;

namespace Knapsack.Items {
	public abstract class BaseActionable : BaseItem, IActionable {
		protected Dictionary<string,Action<Data>> actions;

		public BaseActionable() {
			actions = new Dictionary<string,Action<Data>>();
		}

		public Dictionary<string,Action<Data>> Actions {
			get {
				return actions;
			}
		}

		public void Invoke(string key,Data data) {
			this.InvokeDefault(key,data);
		}

		public override string ToString() {
			return this.ToStringDefault(base.ToString());
		}
	}
}
