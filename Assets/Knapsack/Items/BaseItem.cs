﻿using System;
using Universe;
using UnityEngine;

namespace Knapsack.Items {
	public abstract class BaseItem {
		protected System.Random Random = new System.Random();

		public PropertyHolder Properties { get; protected set; }

		public BaseItem() {
			Properties = new PropertyHolder();
			Sprite = Resources.Load<Sprite>(SpritePath);
		}

		public virtual string Name {
			get {
				return "Base Item";
			}
		}

		public virtual string Description {
			get {
				return "This is the default description.";
			}
		}

		public virtual int StackSize {
			get {
				return 99;
			}
		}

		public virtual void OnBeforePlaced(Data data) { }

		public virtual void OnBeforeRemoved(Data data) { }

		public virtual void OnAfterPlaced(Data data) { }

		public virtual void OnAfterRemoved(Data data) { }

		public bool Is(Type type) {
			return GetType().IsSubclassOf(type) || GetType() == type;
		}

		public override string ToString() {
			return string.Format("[{0}]\n{1}",Name,Description);
		}

		public int Depth() {
			Type current = GetType();
			int depth = 0;
			while (current != typeof(BaseItem)) {
				current = current.BaseType;
				depth++;
			}
			return depth;
		}

		protected abstract string SpritePath { get; }
		public Sprite Sprite { get; private set; }
	}
}