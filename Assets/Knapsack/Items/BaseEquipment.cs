﻿using System;
using Knapsack.Attributes;
using Universe;

namespace Knapsack.Items {
	public abstract class BaseEquipment : BaseItem, IEquippable, IDamageable {
		public BaseEquipment() : base() {
			Properties.Add("durability",MaxDurability);
			Depth();
		}

		public override string Name {
			get {
				return "Base Equipment";
			}
		}

		public override int StackSize {
			get {
				return 1;
			}
		}

		public virtual int MaxDurability {
			get {
				return 100;
			}
		}

		public override void OnAfterPlaced(Data data) {
			ItemStack stack = data.Read<ItemStack>("stack");
			int slot = stack.Holder.GetSlot(stack);
			base.OnAfterPlaced(data);
			if(stack.Holder.SlotRestrictions[slot] == typeof(BaseEquipment)) {
				OnEquip(data);
			}
		}

		public override void OnBeforeRemoved(Data data) {
			ItemStack stack = data.Read<ItemStack>("stack");
			int slot = stack.Holder.GetSlot(stack);
			base.OnBeforeRemoved(data);
			if(stack.Holder.SlotRestrictions[slot] == typeof(BaseEquipment)) {
				OnUnequip(data);
			}
		}

		public virtual void OnEquip(Data data) { }

		public virtual void OnUnequip(Data data) { }

		public override string ToString() {
			return base.ToString() + string.Format("\n- [Equippable]\n  - Max durability: {0}",MaxDurability);
		}

		string[] damages = new string[] {
			"slightly",
			"a lot",
			"significantly"
		};
		public virtual void Damage(Data data, int amount) {
			int magnitude = (int)Math.Log10(amount);
			if(magnitude < 0)
				magnitude = 0;
			if(magnitude >= damages.Length)
				magnitude = damages.Length - 1;

			Console.WriteLine(string.Format("Your {0} is looking {1} more damaged.", Name, damages[magnitude]));

			this.DamageDefault(data, amount);
		}

		public virtual void OnDestroyed(Data data) {
			this.OnDestroyedDefault(data);
		}
	}
}
