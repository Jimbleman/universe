﻿using Universe;
namespace Knapsack {
	public class Knapsack {
		// universe returns base data containing world, etc
		// knapsack returns base data with knapsack-related data attached
		public static Data RequestData(ItemStack stack) {
			return Universe.Universe.RequestData("stack",stack);
		}
	} 
}
