﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Universe;
using Knapsack;
using UnityEngine.UI;

[RequireComponent(typeof(InventoryComponent))]
public class GUIInventory : MonoBehaviour {
	public Inventory Source;
	public Text Title;
	public Transform SlotHolder;
	private GUIInventorySlot[] Slots;

	public void Awake() {
		Title.text = "Inventory";
		Slots = GetComponentsInChildren<GUIInventorySlot>();
	}
	public void Start() {
		Source = GetComponent<InventoryComponent>().Source;
	}

	public void Update() {
		for(int i = 0; i < Slots.Length; i++) {
			GUIInventorySlot slot = Slots[i];
			if (Source[i].Empty()) {
				slot.ItemSprite.sprite = null;
				slot.Quantity.text = "";
				slot.ItemSprite.color = Color.clear;
			} else {
				slot.ItemSprite.sprite = Source[i].ItemType.Sprite;
				slot.Quantity.text = Source[i].Quantity.ToString();
				slot.ItemSprite.color = Color.white;
			}
		}
	}
}
