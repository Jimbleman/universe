﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;
using System;
using Knapsack;

public class GUIInventorySlot : MonoBehaviour, IPointerClickHandler {

	Inventory Source;
	int Index;
	public Image ItemSprite;
	public Text Quantity;

	public void Awake() {
		Index = transform.GetSiblingIndex();
	}
	public void Start() {
		Source = transform.GetComponentInParent<InventoryComponent>().Source;
	}

	public void OnPointerClick(PointerEventData eventData) {
		print(eventData.button + " " + Index + ": " + Source[Index].ToString());
	}
}
