﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Knapsack;
using Knapsack.Items;

public class BasicInventory : InventoryComponent {
	public int Size = 5;
	public override void Awake () {
		Source = new Inventory(5,false);
	}
}
