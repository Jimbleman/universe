﻿using System;
using Knapsack;
using Knapsack.Items;
using Knapsack.Attributes;

public class ItemCarrot: BaseItem {
	public ItemCarrot() : base() {
	}
	public override string Name {
		get {
			return "Carrot";
		}
	}
	public override string Description {
		get {
			return "A large orange carrot with a fluffy green top. It's probably delicious, but I wouldn't know.";
		}
	}
	protected override string SpritePath {
		get {
			return "textures/items/itemcarrot";
		}
	}
}