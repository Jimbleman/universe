﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Knapsack;

public class Game : MonoBehaviour {
	public InventoryComponent inv;
	Inventory inventory;// = new Inventory(0, true);
	void Start () {
		inventory = inv.Source;
		inventory.Expand(5);
		ItemStack stack1 = new ItemStack(Items.Carrot,40);
		ItemStack stack2 = new ItemStack(Items.Carrot,30);
		ItemStack stack3 = new ItemStack(Items.Carrot,20);
		ItemStack stack4 = new ItemStack(Items.Carrot,10);

		inventory.Scatter(stack1,stack2,stack3,stack4);

		//print(inventory.ToString());

		inventory.Sort();

		//print(inventory.ToString());
	}
}
